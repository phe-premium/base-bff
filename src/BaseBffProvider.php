<?php

namespace Centralpos\BaseBff;

use Centralpos\BaseBff\ServicesClient\ServiceClientManager;
use Firebase\JWT\JWT;
use GuzzleHttp\Client;
use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Support\ServiceProvider;

class BaseBffProvider extends ServiceProvider
{

    const KEYCLOAK_CACHE_KEY = 'keycloak_public_key';

    public function register()
    {
        $this->app->configure('database');
    }

    public function boot()
    {
        $this->userAuth();
        $this->defineGate();
        $this->addLogstashChannel();
    }

    protected function defineGate()
    {
        $this->app[Gate::class]->before(function ($user, $ability) {
            return in_array($ability, $user->realm_access->roles);
        });
    }

    protected function addLogstashChannel()
    {
        $this->app->configure('logging');

        $loggingChannels = array_merge(
            $this->app['config']->get('logging.channels', []),
            require realpath(__DIR__ . '/../config/logging.php')
        );

        $this->app['config']->set('logging.channels', $loggingChannels);
    }

    protected function userAuth()
    {
        $this->app['auth']->viaRequest('api', function ($request) {

            try {
                $decodedToken = JWT::decode(
                    $request->bearerToken(),
                    $this->buildKeycloakPublicKey(),
                    ['RS256']
                );

                return new GenericUser((array) $decodedToken);
            } catch (\Exception $exception) {
                return null;
            }
        });
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    protected function buildKeycloakPublicKey()
    {
        $publicKey = wordwrap($this->getKeycloakPublicKey(), 64, "\n", true);

        return "-----BEGIN PUBLIC KEY-----\n"
            . $publicKey
            . "\n-----END PUBLIC KEY-----";
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    protected function getKeycloakPublicKey()
    {
        if (!cache()->has(self::KEYCLOAK_CACHE_KEY)) {

            $client = new Client();
            $response = $client->get(env('KEYCLOAK_REALM_URL'));

            $config = json_decode($response->getBody(), true);

            cache()->put(
                self::KEYCLOAK_CACHE_KEY,
                $config['public_key'],
                env('KEYCLOAK_CACHE_TTL', 3600)
            );
        }

        return cache(self::KEYCLOAK_CACHE_KEY);
    }
}
